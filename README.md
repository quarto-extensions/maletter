# Quarto Vorlage für Briefe

Eine Vorlage für Briefe, die auf `scrlttr2` basiert.


## Installieren

Die Installation in ein bestehendes Projekt erfolgt über folgende Kommandozeile im Terminal:

```bash
quarto add https://gitlab.com/quarto-extensions/maletter/-/archive/main/shorty-main.zip
```

Um eine Template-Installation durchzuführen, die auch Templates enthält, verwende folgende Befehlszeile.
Dieser Schritt erstellt einen neuen Ordner, der die Vorlagen und die Erweiterung enthält.

```bash
quarto use template https://gitlab.com/quarto-extensions/maletter/-/archive/main/shorty-main.zip
```


## Einsatz

Nutze folgende Einstellung in dem `yaml-header`:

```yaml
format: maletter-pdf
```


## Beispiele

Ein Beispiel befindet sich im Repo:

- [Quellcode](example-maletter.qmd)
- [Vorschau](example-maletter.pdf)
